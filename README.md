# :warning: OUTDATED :warning:

Go here: https://gitlab.com/Xonlly/tracktl-watcher


# Last doc:

tracktl-watcher
===============

This script allows to be notified of the state of the queue in a  http://track.tl  party.

[demo page](http://watcher.tracktl.com/)

# Requirements

The script use es6 promise, you may consider including a polyfill.

Build the source with `npm install && npm run build:example` and include the file [watcher.js](./dist/watcher.js), 

or use the built version: http://watcher.tracktl.com/watcher.js


# Usage

```javascript
var watcher = window.watcher

watcher.connect( partyId )

    .then( function(){

        // connected to the party

        // get the selected track ( the one which is either paused or palying )
        var selected = watcher.trackStore.getSelected()

        selected.title
        selected.artist
        selected.vote
        ..

        // get the ordered queue of next tracks
        var queue = watcher.trackStore.getFullQueue()

        // be notified when the queue change
        watcher.trackStore.on( 'change', function() {
            // the queue as changed ( reordered, track added, track removed, track altered ... )
        })



        // get playing information

        // return true if the player is playing
        watcher.mainPlayer.getPlaying()

        // be notified when the player change
        watcher.mainPlayer.on( 'change', function() {

        })
    })


```

see also the [demo](./example/app.js)

# Spec

```javascript
watcher.trackStore.getSelected()
// return a track :
// {
//      id,
//
//      title,
//      artist,
//      duration,           // ( in second )
//      coverArt,
//      coverArt_big,
//
//      vote,
//
//      adder : {
//          id,
//          name,
//          pic,
//      }
// }

watcher.trackStore.getFullQueue()
// return a array of tracks

watcher.mainPlayer.getPlaying()
// return a boolean

watcher.mainPlayer.getPosition()
// return the track playing position ( in second )

```