import listen               from 'engine.io-client'
import EventEmitter         from 'events'
/**
 *
 * handle
 *   - reconnection
 *   - event subscription ( with promise mecanism )
 *
 */


const genUID = () =>
    Math.random().toString(34).slice(2)

export const create = ( options = {} ) => {

    const ee = new EventEmitter
    const requests = {}


    let socket
    let reconnectTimeout

    const socketBinding = () =>
        socket
            .on('error', err => ee.emit('error', err ))
            .on('open', () => {
                clearTimeout( reconnectTimeout )
                ee.emit('open')
            })
            .on('close', () => {

                // notify
                ee.emit('disconnect' )

                // reconnect
                cleanUp()
                clearTimeout( reconnectTimeout )
                reconnectTimeout = setTimeout( connect, 1000 )
            })
            .on('message', m => {

                let {type, payload, meta} = JSON.parse(m)
                payload = payload || {}

                // is the event a response
                if ( meta && meta.key in requests ) {

                    const { reject, resolve } = requests[ meta.key ]

                    delete requests[ meta.key ]

                    payload.error

                        ? reject( payload )

                        : resolve( payload )

                }

                // notify
                ee.emit( type, payload )
            })

    // launch a new connection,
    // set the closure var to the new socket
    // listen to event and relay to the handlers
    const connect = () =>
        new Promise(( resolve, reject ) => {

            socket = listen(
                `ws://${ options.hostname || 'localhost' }${ options.port ? ':'+options.port : ''  }`,
                {
                    upgrade     : options.upgrade,
                    transports  : options.transports,
                }
            )

            socketBinding( socket )

            ee
                .once( 'error' , err => reject( err ) )
                .once( 'ready', () => resolve() )
        })




    // if exist, clean up the socket
    const cleanUp = () => {
        if ( !socket )
            return
        socket.removeAllListeners()
        socket.close()
        socket = null
    }





    const api = {

        connect,

        /**
         * unsubcribe to an event
         *
         * @return {function}   function to call to unsubcribe
         */
        on : ( type, handler ) => {

            ee.on( type, handler )

            return api
        },


        /**
         * unsubcribe to the event
         */
        off : ( type, handler ) => {

            ee.off( type, handler )

            return api
        },

        /**
        * emit an event
        * act as a promise
        * /!\ for the promise to resolve, the server must implement a response with meta key for this event
        */
        emit : ( type, payload ) =>

            new Promise( (resolve, reject) => {

                const key = genUID()

                requests[ key ] = { resolve, reject }

                socket && socket.send( JSON.stringify({ type, payload, meta:{ key } }) )
            })
        ,

        /**
         * return the socketId
         */
        id : () =>
            socket && socket.id
        ,


        /**
         * clean up
         */
        destroy : () => {
            ee.removeAllListeners()
            cleanUp()
        },

        _close : () => {
            socket.close()
        },
    }

    return api
}