
const buildUserPicUrl = uri =>
    `https://res.cloudinary.com/jukeo-net/image/${uri}`

const formatTrack = (users, x) => {
    const adder = users.find( ({id}) => id === x.adderId )
    return {
        artist          : x.song.artist,
        title           : x.song.name,
        vote            : x.votes.length,
        coverArt        : x.song.coverArt,
        coverArt_big    : x.song.coverArt_big,
        duration        : x.song.duration,
        id              : x.id,
        adder           : {
            ...adder,
            pic : buildUserPicUrl( adder.pic )
        }
    }
}

export const getSelected = ({ tracks, users, played }) =>
    played && played.id && formatTrack( users, tracks.find( ({id}) => id === played.id ) )

export const getFullQueue = ({ tracks, users, played }) =>
    tracks
        .filter( ({ id }) => id != ( played && played.id ) )
        .sort( (a,b) => {

            if ( (!!a.priority) != (!!b.priority) )
                return a.priority ? -1 : 1

            if ( a.votes.length != b.votes.length )
                return a.votes.length < b.votes.length ? 1 : -1

            return (+a.id) > (+b.id) ? 1 : -1
        })
        .map( formatTrack.bind( null, users ) )


export const getPlaying = ({ played }) =>
    !!(played && played.playing)
        
export const getPosition = ({ played, tracks }) => {
    if ( !played || !played.id )
        return 0
        
    return played.playing
        ? Math.min( tracks.find( ({ id }) => id === played.id ).song.duration, Date.now() - played.position ) / 1000
        : played.position / 1000
}