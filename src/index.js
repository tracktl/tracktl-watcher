import {create}             from './watcher'
import * as config          from './config'

const watcher = create( config )

export default watcher
if ( 'undefined' != typeof window )
    window.watcher = watcher