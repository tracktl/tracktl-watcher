import {create}                     from '../watcher'
import * as config                  from '../config'
import regeneratorRuntime           from 'regenerator-runtime'

const partyId   = 25391

describe('connect',function(){
    
    it('should connect', async function(){
        
        const watcher = create( config )
        
        await watcher.connect( partyId )
        
    })
})

describe('change listener',function(){
    
    it('should expose eventEmitter interface', async function(){
        
        const watcher = create( config )
                        
        expect( watcher.trackStore.on ).toBeTruthy()
        expect( watcher.trackStore.removeListener ).toBeTruthy()
        
        expect( watcher.mainPlayer.on ).toBeTruthy()
        expect( watcher.mainPlayer.removeListener ).toBeTruthy()
    })
})

describe('gettter',function(){
    
    beforeEach( async function(){
        
        this.watcher = create( config )
        
        await this.watcher.connect( partyId )
        
    })
    
    it('should get the selected track', function(){
        const track = this.watcher.trackStore.getSelected()
        
        expect( track.title ).toBeTruthy()
        expect( track.artist ).toBeTruthy()
        expect( track.vote ).toBeTruthy()
        expect( track.coverArt ).toBeTruthy()
        expect( track.coverArt_big ).toBeTruthy()
        expect( track.duration ).toBeTruthy()
        expect( track.adder.name ).toBeTruthy()
        expect( track.adder.pic ).toBeTruthy()
    })
    
    it('should get the queued tracks', function(){
        const tracks = this.watcher.trackStore.getFullQueue()
            
        expect( tracks.length ).toBeGreaterThan( 0 )
        
        tracks.forEach( track => {
            
            expect( track.title ).toBeTruthy()
            expect( track.artist ).toBeTruthy()
            expect( track.vote ).toBeTruthy()
            expect( track.coverArt ).toBeTruthy()
            expect( track.coverArt_big ).toBeTruthy()
            expect( track.duration ).toBeTruthy()
            expect( track.adder.name ).toBeTruthy()
            expect( track.adder.pic ).toBeTruthy()
            
        })
    })
    
    it('should get the selected track playing state', function(){
        const playing = this.watcher.mainPlayer.getPlaying()
        expect( playing ).not.toBeNull()
    })
    it('should get the selected track position', function(){
        const position = this.watcher.mainPlayer.getPosition()
        expect( typeof position ).toBe( 'number' )
    })
})