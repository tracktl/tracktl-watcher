import {create as createSocket}     from './socket'
import EventEmitter                 from 'events'
import regeneratorRuntime           from 'regenerator-runtime'
import * as projector               from './projector'

const initHandlers = ( socket, eventEmitter, state ) => {
            
    socket.on('trackList:init', data => {
        
        Object.assign( state, data )
        
        eventEmitter.emit('change')
    })
    
    socket.on('userList:patch', data => {
        
        Object.assign( state, {
            users : state.users.map( user => {
                const u = data.users && data.users.find( ({ id }) => user.id == id )
                
                return {
                    ...user,
                    ...u,
                }
            })
        })
        
        eventEmitter.emit('change')
    })
    
    socket.on('trackList:patch', data => {
        
        Object.assign( state, {
            played :
                data.played
                    ? { ...state.played, ...data.played }
                    : state.played
            ,
            tracks : [
                ...state.tracks
                    .filter( ({ id }) => !data.removeIds || !data.removeIds.some( u => u == id ) )
                    .map( track => {
                        
                        const t = data.tracks && data.tracks.find( ({ id }) => track.id == id )
                        
                        return {
                            ...track,
                            ...t,
                        }
                    })
                ,
                ...( data.tracks || [] )
                    .filter( ({ id }) => !state.tracks.some( u => u.id == id ) )
            ],
        })
        
        eventEmitter.emit('change')
    })
}


export const create = ( config ) => {
    
    const eventEmitter = new EventEmitter
    const state = {
        tracks      : [],
        users       : [],
        played      : {},
    }
    const socket = createSocket({
        hostname    : config.hostname,
        port        : config.port,
    })
    
    const connect = async partyId => {
        
        await socket.destroy()
        
        await socket.connect()
        
        initHandlers( socket, eventEmitter, state )
        
        await socket.emit('party:join', {partyId})
        
        await socket.emit('trackList:get')
    }
    
    const api = eventEmitter
    api.connect = connect
    api.trackStore = new EventEmitter
    api.trackStore.getSelected  = projector.getSelected.bind( null, state )
    api.trackStore.getFullQueue = projector.getFullQueue.bind( null, state )

    api.mainPlayer = new EventEmitter
    api.mainPlayer.getPlaying   = projector.getPlaying.bind( null, state )
    api.mainPlayer.getPosition  = projector.getPosition.bind( null, state )
    
    eventEmitter.on('change', () => {
        api.mainPlayer.emit('change')
        api.trackStore.emit('change')
    })
    
    return api
}