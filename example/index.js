require('file-loader?name=index.html!./index.html')

import ReactDOM             from 'react-dom'
import React                from 'react'
import {AppState as App}    from './app.js'


ReactDOM.render( <App watcher={ window.watcher } />, document.getElementById('app') )
