const path = require('path')
const webpack = require('webpack')

const production = 'production' == process.env.NODE_ENV

module.exports = {

    entry   : {
        'watcher-example'       : './example/index.js',
        'watcher'               : './src/index.js',
    },
    
    output  : {
        path        : './dist', //path.join(__dirname, 'dist'),
        filename    : '[name].js'
    },

    module  : {

        rules: [
            {
                test: /\.js$/,
                use : [
                    {
                        loader : 'babel-loader',
                    },
                ],
            },
        ],
    },

    plugins : production
        ? [
            // minify
            new webpack.optimize.UglifyJsPlugin({ compress: {warnings: false} }),
        ]
        : []
    ,

    devtool : 'source-map',
}