import React    from 'react'

export class AppState extends React.Component {
    
    constructor( props ){
        super( props )
        
        this.state = {
            status      : 'not connected',
            tracks      : [],
            selected    : null,
        }
        
        this._update = () =>
            this.props.watcher &&
                this.setState({
                    tracks      : this.props.watcher.trackStore.getFullQueue(),
                    selected    : this.props.watcher.trackStore.getSelected(),
                    
                    playing     : this.props.watcher.mainPlayer.getPlaying(),
                })
    }
    
    connect ( partyId ){
        this.setState({ status: 'connecting' })
        
        this.props.watcher.connect( partyId )
            .then(() => this.setState({ status: 'connected' }) )
            .catch( error => this.setState({ status: 'failed' }) )
    }
    
    componentDidMount(){
        
        // ensure to not listen twice
        this.props.watcher.trackStore.removeListener('change', this._update )
        this.props.watcher.mainPlayer.removeListener('change', this._update )
        
        this.props.watcher.trackStore.on('change', this._update )
        this.props.watcher.mainPlayer.on('change', this._update )
    }
    
    componentWillUnmount(){
        this.props.watcher.trackStore.removeListener('change', this._update )
        this.props.watcher.mainPlayer.removeListener('change', this._update )
    }
        
    render() {
        return <App {...this.state} connect={ partyId => this.connect( partyId )} />
    }
}

const App = props =>
    <div>
        <span>{props.status}</span>
        
        { props.status == 'not connected' &&
            <div>
                <input type="text" defaultValue="68015" placeholder="party id (ex 68015 )"></input>
                <button onClick={ e => props.connect( e.target.parentNode.firstElementChild.value ) } >go</button>
            </div>
        }
        
        { props.status == 'connected' &&
            <Player {...props} />
        }
        { props.status == 'connected' &&
            <List {...props} />
        }
    </div>
    
const List = ({ tracks }) =>
    <ul>
        {tracks.map( track =>
            <Track key={track.id} { ...track } />
        )}
    </ul>


const Track = ({ adder, title, vote, coverArt }) =>
    <li>
        <span>
            <span><img src={ coverArt } width={20} height={20} /></span>
            <span>{ title }</span>
            <span>[ vote: { vote } ]</span>
        </span>
        
        <span>
            <span> -- added by { adder.name }</span>
            <img src={ adder.pic } width={20} height={20} />
        </span>
    </li>
    
    
const Player = ({ selected, playing }) =>
    <h1>
        <span dangerouslySetInnerHTML={ {__html: playing ? '&#9654;' : '&#10074;&#10074;'}} />
        <span><Track { ...selected } /></span>
    </h1>
